<?php
?>

<div class="container mt-5 p-5 rounded cart">
    <div class="row no-gutters">
        <div class="col-md-8">
            <div class="product-details ">
                <hr>
                <h6 class="mb-0">Votre Panier</h6>
                <div class="d-flex justify-content-between"><span>Vous avez <?=  $compteur; ?> produit(s) dans votre panier</span>

                </div>
                <?php
                if (isset($_SESSION['cart'])) {
                    $cart = $_SESSION['cart']; ?>
                    <?php foreach ($cart as $item => $value) { ?>

                        <div class="d-flex justify-content-between align-items-center  p-2 items rounded" style="
                     width: 916px;">
                            <div class="d-flex flex-row"><img class="rounded" src="<?= $bonnets[$item]['image'] ?>"
                                                              width="40">
                                <div class="ml-2"><span
                                            class="font-weight-bold d-block"><?= $bonnets[$item]['nom'] ?></span>
                                <p><?= $bonnets[$item]['description'] ?></p></div>
                            </div>
                            <div class="d-flex flex-row align-items-center"><span class="d-block"><?= $value ?></span><span
                                        class="d-block ml-5 font-weight-bold"><?= $value*$bonnets[$item]['prix'] ?></span><i
                                        class="fa fa-trash-o ml-3 text-black-50"></i></div>
                        </div>

                    <?php } ?>
                <?php } ?>


            </div>
        </div>

    </div>
</div>
