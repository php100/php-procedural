<article>

    <main class="container mt-auto">
        <div class="card-group ">
            <?php

            foreach ($bonnets as $bonnet) {

                    ?>


                    <div class="card">
                        <img
                                src="<?php echo $bonnet ["image"] ?>"
                                class="card-img-top"
                                alt="..."
                        />
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $bonnet ["nom"] ?></h5>
                            <p class="card-text">
                                <?php echo $bonnet ["description"] ?>
                            </p>
                            <p class="card-text">
                                <strong> Prix TTC </strong>
                            <p >
                                <?php lineTTC($bonnet ["prix"]) ?></p>

                            <strong> Prix HT </strong>
                            <p>
                                <?php lineTVA($bonnet ["prix"]) ?></p>
                            <small class="text-muted"><a href="<?= '?page=list&addCart='.$bonnet['id']?>" class="btn btn-info mb-3" >Ajoutez au panier</a> </small>
                            </p>
                        </div>
                    </div>
                <?php
            } ?>


        </div>

    </main>
</article>

