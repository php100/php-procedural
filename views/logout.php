<?php

session_unset();
session_destroy();


?>



<div class="d-flex justify-content-center container mt-3 shadow ">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading"> Well done! </h4>
        <p>Vous vous êtes correctement déconnectéE de votre espace personnel.</p>
        <hr>
        <p class="mb-0">Vous pouvez vous reconnecter <a href="?page=login">ICI </a> </p>
    </div>
</div>










