
<?php function loginOK()
{ ?>
    <div class="d-flex justify-content-center container mt-3">
        <div class="alert alert-success" role="alert">
            Connexion réussie
        </div>
    </div>

<?php } ?>
<?php function loginFail()
{ ?>
    <div class="d-flex justify-content-center container mt-3">
        <div class="alert alert-danger" role="alert">
            Login ou mot de passe incorrect
        </div>
    </div>

<?php } ?>

<div class="col-6 card shadow mx-auto mt-5 pb-5">
    <?php
    if (!empty($_POST)) {
        if ($password === $_POST['password']) {
            $_SESSION['login'] = $_POST['login'];
            loginOk();
        } else {
            loginFail();
        }
    }
    ?>
    <h3 class="text-center mt-3">Connectez vous à votre espace perso !</h3>
    <form class="form-group" method="POST">

        <div>
            <label for="login">Login</label>
            <input type="login" class="form-control" id="login" name="login" placeholder="Enter login">

        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                   placeholder="Password">
        </div>
        <div class="text-center mt-3">

            <button type="submit" class="btn btn-primary ">Submit</button>
        </div>

    </form>
</div>










