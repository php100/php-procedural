<?php

$bonnets = [
    [
        "id" => 0,
        "nom" => "Bonnet en laine",
        "prix" => 10,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "image" => "https://www.bibop-et-lula.com/4225-large_default/bonnet-laine-enfant-gris-souris.jpg"
    ],

    [
        "id" => 1,
        "nom" => "Bonnet de chat",
        "prix" => 14,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "image" =>"http://cdn.shopify.com/s/files/1/0258/5470/5746/products/bonnet_chat_bleu_1200x1200.jpg?v=1573208600"
    ],
    [
        "id" => 2,
        "nom" => "Bonnet en laine et cachemire",
        "prix" => 20,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "image" => "https://larenardeapprivoisee.fr/wp-content/uploads/2017/11/6570210_bonnet_cachemire-small.jpg"
    ],
    [
        "id" => 3,
        "nom" => "Bonnet arc-en-ciel",
        "prix" => 12,
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Duis a leo diam. Quisque lorem orci, accumsan quis dolor sed, gravida.",
        "image" => "https://www.chapeauxetcasquettes.fr/images/products/large/504539.jpg"
    ],
];

//global $password;
$password = 'azerty';

