<?php
session_start();
require_once 'includes/function.php';
require_once 'includes/variable.php';
$cart= [];
$compteur = 0;
if (isset($_SESSION['cart'])) {
    $cart = $_SESSION['cart'];
}
if (isset($_GET['addCart'])){
    if(!isset($cart[$_GET['addCart']])){
        $cart[$_GET['addCart']]= 0;

    }
    $cart[$_GET['addCart']] ++ ;


}
foreach ($cart as $item => $value) {
    $compteur += $value;
}

$_SESSION['cart'] = $cart;
//git status
var_dump($cart);


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
              integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="./css/style.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" >Paye ton bonnet</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link <?= $currentPage == 'home' ? ' active' : '' ?>" href="?page=home">Home </a>
            </li>
            <li class="nav-item">
                <a class="nav-link<?= $currentPage == 'list' ? ' active' : '' ?>" href="?page=list"> Tous les bonnets</a>
            </li>
            <li class="nav-item">
                <a class="nav-link<?= $currentPage == 'cart' ? ' active' : '' ?>" href="?page=cart"> Votre Panier</a>
            </li>
            <li class="nav-item">
                <a class="nav-link<?= $currentPage == 'login' ? ' active' : '' ?>" href="?page=login">Connexion</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $currentPage == 'logout' ? ' active' : '' ?>" href="?page=logout">Déconnexion</a>
            </li>
        </ul>
        <div class="navbar-nav mr-auto">
            <p>  Bonjour :   </p>
            <p> <?php
                if (!empty($_SESSION['login'])) {
                    echo $_SESSION['login'];
                }
                ?></p>
        </div>

        <span class="navbar-text">
      C'est l'hiver enfile ton bonnet que je te casse les dents
    </span>
    </div>
</nav>

<!--</body>-->
<!--</html>-->
