<?php


?>

<?php require_once('includes/header.php'); ?>



<article>
    <div>
        <h2> Exercice 2 </h2>
        <div>
            <table class="table ">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prix</th>
                    <th>Description</th>

                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($bonnets as $bonnet) { ?>
                    <tr>
                        <?php foreach ($bonnet as $item) { ?>

                            <td><?php echo $item ?></td>
                        <?php } ?>
                    </tr>

                <?php } ?>
                </tbody>


            </table>
        </div>
    </div>
    <div>
        <h2> Exercice 3 Prix & Bonnets </h2>
        <div>
            <table class="table ">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prix</th>
                    <th>Prix</th>

                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($bonnets as $bonnet) {
                    ?>
                    <tr>
                        <td><?php echo $bonnet ["nom"] ?> </td>
                        <td> <?php if ($bonnet ["prix"] <= 12) { ?>
                                <span class="text-success"> <?php echo $bonnet ["prix"] ?></span>
                            <?php } else if ($bonnet ["prix"] > 12) { ?>
                                <span class="text-primary"> <?php echo $bonnet ["prix"] ?></span> <?php } ?> </td>
                        <td><?php echo $bonnet ["description"] ?> </td>


                    </tr>

                <?php } ?>


                </tbody>


            </table>
        </div>
    </div>

    <div>
        <h2> Exercice 4 Prix HT et TTC </h2>
        <div>
            <table class="table ">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prix TTC</th>
                    <th>Prix HT</th>
                    <th>Description</th>

                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($bonnets as $bonnet) {
                    ?>
                    <tr>
                        <td><?php echo $bonnet ["nom"] ?> </td>



                        <td> <?php if ($bonnet ["prix"] <= 12) { ?>
                                <span class="text-success"> <?php echo $bonnet ["prix"] ?></span>
                            <?php } else if ($bonnet ["prix"] > 12) { ?>
                                <span class="text-primary"> <?php echo $bonnet ["prix"] ?></span> <?php } ?> </td>

                        <td><?php echo TVA($bonnet ["prix"]) ?></td>
                        <td><?php echo $bonnet ["description"] ?> </td>


                    </tr>

                <?php } ?>


                </tbody>


            </table>
        </div>
    </div>

    <div>
        <h2> Exercice 5 Prix HT et TTC </h2>
        <div>
            <table class="table ">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prix TTC</th>
                    <th>Prix HT</th>
                    <th>Description</th>

                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($bonnets as $bonnet) {
                    ?>
                    <tr>
                        <td><?php echo $bonnet ["nom"] ?> </td>



                        <?php  lineTTC($bonnet ["prix"]) ?>

                        <?php  lineTVA($bonnet ["prix"]) ?>
                        <td><?php echo $bonnet ["description"] ?> </td>


                    </tr>

                <?php } ?>


                </tbody>


            </table>
        </div>
    </div>
</article>

<?php require_once('includes/footer.php'); ?>




