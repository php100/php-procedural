<?php



require_once 'includes/variable.php';
require_once 'includes/function.php';


// si la road est vide, on va sur home
if (empty($_GET['page'])) {
    $_GET['page'] = 'home';
}

$currentPage = $_GET['page'];

switch ($currentPage) {
    case 'list':
        $pageTitle = "Paye ton Bonnet ";
        break;
    case 'login':
        $pageTitle = "Connexion à votre Espace";
        break;
    case 'logout':
        $pageTitle = "Déconnexion";
        break;
    case 'cart':
        $pageTitle = "Votre Panier";
        break;

    default:
        $pageTitle = "Bloup!";
        break;
}




include "includes/header.php";

include 'views/' . $currentPage . '.php';

include "includes/footer.php";
